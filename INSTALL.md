---
How to install VIM Toolset
---

- Wise users will make backups of a few sundry items.
    * ${HOME}/.vim
    * ${HOME}/.vimrc
- make a directory somewhere for the installer directory
- cd into the directory you just made
- git clone https://gitlab.com/vim_toolset/vim-toolset-installer.git
- cd vim-toolset-installer
- ./install
    * follow the prompts
    * most users will want to use the original repositories
        * Did the original go away?  Opt for non-originals and use my mirror
          copies of them. If you can clone this, you can clone them.
    * most users will want to opt "N" for ssh
        * If you opt yes, then you should have a valid ssh key set up and an
          account on gitlab and github and have commit access to the projects.
    * Want it all?  Press Y/y for everything
        * Aww, you picked No?  You get pathogen installed with whatever else
          you picked because this doesn't work without it!
    * Do you have stuff that exists and are being prompted about replacement?
	* WARNING: This is going to DELETE your stuff.  You did make that
	  backup we talked about since you're a wise user right? If you didn't
          make that backup never fear - you can still choose N to retain your
          original data.

If you had no .vimrc if your own one will be made for you. If you had one, your
old .vimrc will be at .vimrc.old and a few things will be appended to the start
of your new .vimrc to make pathogen work.

---
Powerline Fonts
---
You may notice that the installer will download the powerline fonts, however it
will not install them.

The reason for this is because, at this time, I do not have a good plan for
supporting all the various operating systems and how to install fonts.

This being said - if your operating system, such as OS X, does not provide
packages with Powerline fonts, then you can grab the fonts from the fonts
directory in ~/vim_toolset/plugins and install them.

* Fedora provides the "powerline" package.
* BSD provides "media-fonts/powerline-symbols"

When all else fails - just install from the downloaded repository.

---
Special Notes
---

* Molokai in the original repository has a very dull off-black/grey background.
  If you are like me and this drives you nuts, there are two ways to fix it.
    * _Nuclear Option:_ Choose mirrors instead of originals and it will default
      to my version with a black background.
    * _Savvy Power User:_ Go into the ~/.vim_toolset directory and change the
      repository there to point at my mirror and check out branch trueblack.

      
